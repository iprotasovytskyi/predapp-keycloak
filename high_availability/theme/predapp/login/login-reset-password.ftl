<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "title">
    ${msg("emailForgotTitle")}
    <#elseif section = "form">
    <div class="login_block">
        <form class="login_form" action="${url.loginAction}" method="post">
            <div class="field inp_field">
                <label class="label" for="username">${msg("usernameOrEmail")}</label>
                <input class="inp" type="text" id="username" name="username" autofocus/>
            </div>
            <div class="field">
                <button class="btn">${msg("doSubmit")}</button>
            </div>
        </form>
    </div>
    </#if>
</@layout.registrationLayout>
