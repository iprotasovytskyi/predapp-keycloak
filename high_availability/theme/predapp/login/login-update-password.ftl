<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "title">
    ${msg("updatePasswordTitle")}
    <#elseif section = "form">
    <div class="login_block">
        <form class="login_form" action="${url.loginAction}" method="post">
            <div class="field inp_field">
                <label class="label" for="password-new">${msg("passwordNew")}</label>
                <input class="inp" type="password" id="password-new" name="password-new" autofocus autocomplete="off"/>
            </div>
            <hr class="divider"/>
            <div class="field inp_field">
                <label class="label" for="password-confirm">${msg("passwordConfirm")}</label>
                <input class="inp" type="password" id="password-confirm" name="password-confirm" autocomplete="off"/>
            </div>
            <div class="field">
                <button class="btn">${msg("doSubmit")}</button>
            </div>
        </form>
    </div>
    </#if>
</@layout.registrationLayout>
