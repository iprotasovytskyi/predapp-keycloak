<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
            ${msg("loginTitle",(realm.name!''))}
    <#elseif section = "form">
        <div class="login_block">
            <form class="login_form" action="${url.loginAction}" method="post">
                <div class="field inp_field">
                    <label class="label" for="username">Username</label>
                    <input class="inp" type="text" id="username" name="username" value="${(login.username!'')?html}"
                           autofocus/>
                </div>
                <hr class="divider"/>
                <div class="field inp_field">
                    <label class="label" for="password">Password</label>
                    <input class="inp" id="password" name="password" type="password"/>
                </div>
                <div class="field">
                    <button class="btn">Sign In</button>
                </div>
                <div class="field">
                    <a class="pass_link" href="${url.registrationUrl}">Sign Up</a>
                </div>
                <div class="field">
                    <a class="pass_link" href="${url.loginResetCredentialsUrl}">Forgot Password</a>
                </div>
            </form>
        </div>
    </#if>
</@layout.registrationLayout>
