<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "title">
    ${msg("registerWithTitle")}
    <#elseif section = "form">
    <div class="login_block">
        <form class="login_form" action="${url.registrationAction}" method="post">
            <div class="field inp_field">
                <label class="label" for="email">${msg("email")}</label>
                <input class="inp" type="text" id="email" name="email" autofocus/>
            </div>
            <hr class="divider"/>
            <div class="field inp_field">
                <label class="label" for="firstName">${msg("firstName")}</label>
                <input class="inp" type="text" id="firstName" name="firstName" />
            </div>
            <hr class="divider"/>
            <div class="field inp_field">
                <label class="label" for="lastName">${msg("lastName")}</label>
                <input class="inp" type="text" id="lastName" name="lastName" />
            </div>
            <hr class="divider"/>
            <div class="field inp_field">
                <label class="label" for="password">${msg("password")}</label>
                <input class="inp" type="password" id="password" name="password" />
            </div>
            <hr class="divider"/>
            <div class="field inp_field">
                <label class="label" for="password-confirm">${msg("passwordConfirm")}</label>
                <input class="inp" type="password" id="password-confirm" name="password-confirm" />
            </div>
            <hr class="divider"/>
            <div class="field">
                <button class="btn">${msg("doSubmit")}</button>
            </div>
            <div class="field">
                <a class="pass_link" href="${url.loginUrl}">${msg("backToLogin")}</a>
            </div>
        </form>
    </div>
    </#if>
</@layout.registrationLayout>
