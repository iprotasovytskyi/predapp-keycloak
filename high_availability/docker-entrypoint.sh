#!/bin/bash

if [ $KEYCLOAK_USER ] && [ $KEYCLOAK_PASSWORD ]; then
    keycloak/bin/add-user-keycloak.sh --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
fi
databaseToInstall="postgres"

if [ "$databaseToInstall" != "" ]; then
    echo "[KEYCLOAK DOCKER IMAGE] Using the external $databaseToInstall database"
    /bin/sh /opt/jboss/keycloak/bin/change-database.sh $databaseToInstall
else
    echo "[KEYCLOAK DOCKER IMAGE] Using the embedded H2 database"
fi

exec /opt/jboss/keycloak/bin/standalone.sh $@
exit $?