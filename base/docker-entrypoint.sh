#!/bin/bash

if [ $KEYCLOAK_USER ] && [ $KEYCLOAK_PASSWORD ]; then
    keycloak/bin/add-user-keycloak.sh --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
fi

databaseToInstall="postgres"

echo "[KEYCLOAK DOCKER IMAGE] Using the external $databaseToInstall database"

exec /opt/jboss/keycloak/bin/standalone.sh $@
exit $?
